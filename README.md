# Jungle Game

## Game Chess
八隻棋子由強至弱為：象 (elephant)、獅 (lion)、虎 (tiger)、豹 (leopard)、狼 (wolf)、狗 (dog)、貓 (cat)、鼠 (mouse)

## Game Rule
- 棋子可以吃掉同級或較弱的棋子。
- 鼠可以吃掉象，但象卻不可以吃掉鼠。
- 若棋子走進敵方的陷阱，對方任一棋子都可將之吃掉，但己方棋子不能吃對方任一棋子。
## Game Board

R / C | A | B | C | D | E | F | G
--- |--- | --- | --- | --- | --- | --- | ---
1 | 獅 |  |  |  |  |  |  虎
2 |  | 狗 |  |  |  | 貓  |  
3 | 鼠 |  | 豹 |  | 狼 |   |  象
4 |  |  |  |  |  |   |  
5 |  |  |  |  |  |  |  
6 |  |  |  |  |  |   |  
7 | 象 |  | 狼 |  | 豹 |   |  鼠
8 |  | 貓 |  |  |  | 狗  |  
9 | 虎 |  |  |  |  |   |  獅

## Todo List
- [ ] Define all classes
- [ ] Define all attributes
- [ ] Patterns design
