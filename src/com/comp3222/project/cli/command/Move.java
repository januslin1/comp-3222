package com.comp3222.project.cli.command;

import com.comp3222.project.core.*;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.exception.CanNotMoveException;
import com.comp3222.project.core.utility.Pair;

import java.util.List;
import java.util.Optional;

public class Move implements Command {
  @Override
  public String helpMessage() {
    return "Move chess to grid. move <chessId> <gridId>";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }
    try {
      if (args.size() == 2) {
        // Get Chess
        Optional<Chess> chess = GameWorld.getInstance().getCurrentPlayerChess(args.get(0));
        if (!chess.isPresent()) return new Pair<>("Chess not found!", null);

        // Get Grid
        Optional<Grid> grid = GameWorld.getInstance().getGameBoard().getGridById(args.get(1));
        if (!grid.isPresent()) return new Pair<>("Grid not found!", null);

        try {
          chess.get().moveToGrid(grid.get());
          System.out.println(GameWorld.getInstance().getGameBoard().toString());
          GameWorld.getInstance().nextTern();
          return new Pair<>(null, "");
        } catch (CanNotMoveException e) {
          return new Pair<>(e.getMessage(), null);
        }
      }
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
    return new Pair<>("Illegal Argument!", null);
  }
}
