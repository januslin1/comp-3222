package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Current implements Command {
  @Override
  public String helpMessage() {
    return "Check the current info. current <player/round>";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }
    try {
      if (args.size() == 1) {
        if (args.get(0).equals("player")) {
          return new Pair<>(
              "The current player is " + GameWorld.getInstance().getCurrentPlayer() + " !", null);
        } else if (args.get(0).equals("round")) {
          return new Pair<>("Now is round " + GameWorld.getInstance().getRound() + " !", null);
        }
      }
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
    return new Pair<>("Illegal Argument!", null);
  }
}
