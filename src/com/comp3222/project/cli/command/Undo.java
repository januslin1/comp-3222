package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.exception.NoPlayerMoveRecordException;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Undo implements Command {
  @Override
  public String helpMessage() {
    return "Undo chess move. undo";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }
    try {
      GameWorld.getInstance().undoMove();
      return new Pair<>("", GameWorld.getInstance().getGameBoard().toString());
    } catch (NoPlayerMoveRecordException e) {
      return new Pair<>(e.getMessage(), null);
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
  }
}
