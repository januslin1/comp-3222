package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;
import com.comp3222.project.core.Player;

import java.util.List;
import java.util.stream.Collectors;

public class Chess implements Command {
  @Override
  public String helpMessage() {
    return "Check available chess for this player. chess <playerName>";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }

    try {
      if (args.size() == 1) {
        Player player = GameWorld.getInstance().getPlayerByName(args.get(0));

        if (player == null) {
          return new Pair<>("Player not found!", null);
        } else {
          String result =
              GameWorld.getInstance()
                  .getChessList(player)
                  .stream()
                  .map(chess -> chess.getChessId() + "(" + chess.toString() + ")")
                  .collect(Collectors.joining(", "));
          return new Pair<>(null, result);
        }
      } else if (args.size() == 0) {
        String result =
            GameWorld.getInstance()
                .getChessList(GameWorld.getInstance().getCurrentPlayer())
                .stream()
                .map(chess -> chess.getChessId() + "(" + chess.toString() + ")")
                .collect(Collectors.joining(", "));
        return new Pair<>(null, result);
      }
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
    return new Pair<>("Illegal Argument!", null);
  }
}
