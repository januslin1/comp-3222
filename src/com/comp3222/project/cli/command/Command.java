package com.comp3222.project.cli.command;

import com.comp3222.project.core.utility.Pair;

import java.util.List;

public interface Command {
  default String getName() {
    return getClass().getSimpleName().toLowerCase();
  }

  default String[] getAliases() {
    return new String[0];
  }

  String helpMessage();

  default Pair<String, String> handle(List<String> flags, List<String> args) {
    return null;
  }
}
