package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Save implements Command {
  @Override
  public String helpMessage() {
    return "Save the game.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }

    try {

      GameWorld.getInstance().saveGame();
      return new Pair<>(null, "Game saved!");
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
  }
}
