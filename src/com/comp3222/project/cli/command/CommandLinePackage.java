package com.comp3222.project.cli.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommandLinePackage {
  public static final int MAX_COMMAND_NAME_LENGTH;
  static final Map<String, Command> COMMAND_MAP = new HashMap();

  static {
    registryCommand(new Play());
    registryCommand(new Current());
    registryCommand(new Chess());
    registryCommand(new Move());
    registryCommand(new WhereIs());
    registryCommand(new Hints());
    registryCommand(new Board());
    registryCommand(new Undo());
    registryCommand(new Save());
    registryCommand(new Load());
    registryCommand(new Restart());
    registryCommand(new Help());
    registryCommand(new Exit());

    MAX_COMMAND_NAME_LENGTH =
        COMMAND_MAP.keySet().stream().mapToInt(String::length).reduce(0, Math::max);
  }

  static void registryCommand(Command command) {
    COMMAND_MAP.put(command.getName(), command);
    for (String s : command.getAliases()) {
      COMMAND_MAP.put(s, command);
    }
  }

  public static Command resolveCommand(String name) {
    if (COMMAND_MAP.containsKey(name)) return COMMAND_MAP.get(name);
    return null;
  }

  public static List<Command> getAllCommands() {
    return new ArrayList<>(COMMAND_MAP.values());
  }
}
