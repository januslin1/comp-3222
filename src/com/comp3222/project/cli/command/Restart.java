package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Restart implements Command {
  @Override
  public String helpMessage() {
    return "Restart the Game.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }

    try {
      GameWorld.getInstance().gameReset();
      return new Pair<>(null, "");
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
  }
}
