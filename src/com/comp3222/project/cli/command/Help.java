package com.comp3222.project.cli.command;

import com.comp3222.project.core.utility.Pair;

import java.util.List;
import java.util.stream.Collectors;

import static com.comp3222.project.core.utility.Lang.space;
import static com.comp3222.project.cli.command.CommandLinePackage.MAX_COMMAND_NAME_LENGTH;
import static com.comp3222.project.cli.command.CommandLinePackage.resolveCommand;

public class Help implements Command {
  public static String genBriefHelpMessage(Command command) {
    return command.getName()
        + space(MAX_COMMAND_NAME_LENGTH - command.getName().length() + 4)
        + command.helpMessage();
  }

  @Override
  public String helpMessage() {
    return "Show this help message. List out the help message of all available commands.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    System.out.println("Command" + space(1) + "Description");
    if (args.size() == 0) {
      String result =
          CommandLinePackage.getAllCommands()
              .stream()
              .map(Help::genBriefHelpMessage)
              .collect(Collectors.joining("\n"));
      return new Pair<String, String>(null, result);
    } else if (args.size() == 1) {
      Command command = resolveCommand(args.get(0));
      if (command == null) {
        return new Pair<>("command `" + args.get(0) + "` not found", null);
      } else {
        return new Pair<>(null, genBriefHelpMessage(command));
      }
    }
    return null;
  }
}
