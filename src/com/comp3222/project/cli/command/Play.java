package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;
import com.comp3222.project.core.Player;

import java.util.List;

public class Play implements Command {
  @Override
  public String helpMessage() {
    return "Create new Game. play [-r] <Player1Name> <Player2Name>";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (args.size() == 2) {
      if (args.get(0).equals(args.get(1))) {
        return new Pair<>("Player1 and Player2 cannot have the same name!", null);
      }
      if (flags.size() > 0 && flags.contains("r")) {
        if (GameWorld.hasInstance()) {
          try {
            GameWorld.getInstance()
                .changePlayerInfoAndGameReset(
                    new Player(args.get(0)), new Player(args.get(1)), true);
            return new Pair<>(null, "");
          } catch (Exception e) {
            return new Pair<>("Error!", null);
          }
        }
        GameWorld.setInstance(new Player(args.get(0)), new Player(args.get(1)), true);
        return new Pair<>(null, "");
      } else {
        if (GameWorld.hasInstance()) {
          try {
            GameWorld.getInstance()
                .changePlayerInfoAndGameReset(
                    new Player(args.get(0)), new Player(args.get(1)), false);
            return new Pair<>(null, "");
          } catch (Exception e) {
            return new Pair<>("Error!", null);
          }
        }
        GameWorld.setInstance(new Player(args.get(0)), new Player(args.get(1)), false);
        return new Pair<>(null, "");
      }
    }
    return new Pair<>("Illegal Argument!", null);
  }
}
