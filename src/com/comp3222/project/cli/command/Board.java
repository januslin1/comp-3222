package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Board implements Command {
  @Override
  public String helpMessage() {
    return "Print the board map.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }

    try {
      return new Pair<>(null, GameWorld.getInstance().getGameBoard().toString());
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
  }
}
