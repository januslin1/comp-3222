package com.comp3222.project.cli.command;

import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.utility.Pair;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Hints implements Command {
  @Override
  public String helpMessage() {
    return "Get available grids for target chess to move. hints <chessId>";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    if (!GameWorld.hasInstance()) {
      return new Pair<>("The game has not started yet!", null);
    }
    try {
      if (args.size() == 1) {
        // Get Chess
        Optional<Chess> chess = GameWorld.getInstance().getChess(args.get(0));
        if (!chess.isPresent()) return new Pair<>("Chess not found!", null);

        // Get Grid
        Optional<Grid> grid = GameWorld.getInstance().getGameBoard().getGridByChess(chess.get());
        if (!grid.isPresent()) return new Pair<>("Grid not found!", null);

        return new Pair<>(
            null,
            grid.get()
                .getAvaliableGridsToMove()
                .stream()
                .map(grid1 -> grid1.getId())
                .collect(Collectors.joining(", ")));
      }
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
    return new Pair<>("Illegal Argument!", null);
  }
}
