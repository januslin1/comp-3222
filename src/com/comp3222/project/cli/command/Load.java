package com.comp3222.project.cli.command;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Load implements Command {
  @Override
  public String helpMessage() {
    return "Load the saved game.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {

    try {
      GameWorld.loadGame();
      return new Pair<>(null, "Game loaded!");
    } catch (Exception e) {
      return new Pair<>("Error!", null);
    }
  }
}
