package com.comp3222.project.cli.command;

import com.comp3222.project.core.utility.Pair;

import java.util.List;

public class Exit implements Command {
  @Override
  public String helpMessage() {
    return "Exit Application.";
  }

  @Override
  public Pair<String, String> handle(List<String> flags, List<String> args) {
    System.exit(1);
    return new Pair<>(null, "");
  }
}
