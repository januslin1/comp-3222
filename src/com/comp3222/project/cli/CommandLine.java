package com.comp3222.project.cli;

import com.comp3222.project.cli.command.Command;
import com.comp3222.project.core.utility.Pair;
import com.comp3222.project.core.utility.Tuple3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.comp3222.project.cli.command.CommandLinePackage.resolveCommand;

public class CommandLine implements Runnable {
  private Scanner scanner = new Scanner(System.in);

  public CommandLine() {}

  Pair<String, Tuple3<String, List<String>, List<String>>> readCommand() {
    System.out.print("> ");
    System.out.flush();
    String line = scanner.nextLine();
    StringBuilder name = new StringBuilder();
    List<String> args = new ArrayList<>();
    StringBuilder currentArg = new StringBuilder();
    /**
     * find first space in the line before space : name after space : args args split by space ->
     * String[] check "
     */
    final int STATE_FIND_NAME = 1;
    final int STATE_READ_ARGS = 2;
    final int STATE_READ_STRING = 3;
    int state = STATE_FIND_NAME;
    for (char c : line.toCharArray()) {
      switch (state) {
        case STATE_FIND_NAME:
          if (c == ' ' && name.length() != 0) {
            state = STATE_READ_ARGS;
            currentArg = new StringBuilder();
          } else /* not space */ {
            if (c != ' ') name.append(c);
          }
          break;
        case STATE_READ_ARGS:
          if (c == '"') {
            state = STATE_READ_STRING;
            break;
          }
          if (c == ' ') {
            if (currentArg.length() == 0) break;
            args.add(currentArg.toString());
            currentArg = new StringBuilder();
            break;
          }
          currentArg.append(c);
          break;
        case STATE_READ_STRING:
          if (c == '"') {
            state = STATE_READ_ARGS;
            break;
          }
          currentArg.append(c);
          break;
      }
    }
    if (currentArg.length() > 0) {
      args.add(currentArg.toString());
    }
    ArrayList<String> flags = new ArrayList<>(args.size());
    ArrayList<String> params = new ArrayList<>(args.size());
    for (String arg : args) {
      final char[] cs = arg.toCharArray();
      switch (cs.length) {
        case 0:
          return new Pair<>(
              "CommandLine Parser Error: argument of zero length should be ignored", null);
        case 1:
          if (cs[0] == '-') {
            return new Pair<>("empty param of a single `-`", null);
          } else {
            params.add(arg);
          }
          break;
        case 2:
          if (cs[0] != '-') {
            params.add(arg);
            break;
          }
          if (cs[1] == '-') {
            return new Pair<>("empty param after `--`", null);
          }
          flags.add("" + cs[1]);
          break;
        default:
          if (cs[0] != '-') {
            params.add(arg);
            break;
          }
          if (cs[1] == '-') {
            flags.add(arg.substring(2));
            break;
          }
          /* single char flags */
          for (int i = 1; i < cs.length; i++) {
            flags.add("" + cs[i]);
          }
          break;
      }
    }
    return new Pair<>(null, new Tuple3<>(name.toString(), flags, params));
  }

  @Override
  public void run() {
    Pair<String, List<String>> commandPair;
    final boolean[] end = {false};
    while (!end[0]) {
      Pair<String, Tuple3<String, List<String>, List<String>>> read = readCommand();
      if (read.fst != null) {
        System.out.println(read.fst);
      } else {
        // System.out.println(read.snd); //Debug!!
        if (read.snd._1.equals("quit") || read.snd._1.equals("bye") || read.snd._1.equals("exit")) {
          end[0] = true;
          return;
        }
        Command command = resolveCommand(read.snd._1);
        if (command == null) {
          System.out.println("Command `" + read.snd._2 + "` not found!");
        } else {
          Pair<String, String> result = command.handle(read.snd._2, read.snd._3);
          if (result.fst != null) {
            System.out.println(result.fst);
          } else {
            System.out.println(result.snd);
          }
        }
      }
    }
  }
}
