package com.comp3222.project.core;

import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.utility.Container;
import com.comp3222.project.core.utility.Iterator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PlayerMoveRepository implements Container, Serializable {
  private List<PlayerMove> playerMoves = new ArrayList<>();

  @Override
  public Iterator getIterator() {
    return new PlayerMoveIterator();
  }

  public void add(Grid fromGrid, Grid toGrid, Chess movingChess, Optional<Chess> deadChess) {
    playerMoves.add(new PlayerMove(fromGrid, toGrid, movingChess, deadChess));
  }

  public void clearAll() {
    playerMoves.clear();
  }

  public int getSize() {
    return playerMoves.size();
  }

  public void subList(int fromIdx, int toIndx) {
    playerMoves = playerMoves.subList(fromIdx, toIndx);
  }

  private class PlayerMoveIterator implements Iterator, Serializable {
    int index = -1;

    @Override
    public boolean hasNext() {
      int nextIndex = index + 1;
      if (nextIndex > -1 && nextIndex < playerMoves.size()) {
        return true;
      }
      return false;
    }

    @Override
    public boolean hasPrevious() {
      int nextIndex = index - 1;
      if (nextIndex > -1 && nextIndex < playerMoves.size()) {
        return true;
      }
      return false;
    }

    @Override
    public Object next() {
      if (this.hasNext()) {
        index = index + 1;
        return playerMoves.get(index);
      }
      return null;
    }

    @Override
    public Object previous() {
      if (this.hasPrevious()) {
        index = index - 1;
        return playerMoves.get(index);
      }
      return null;
    }

    @Override
    public Object current() {
      if (playerMoves.size() > 0) {
        return playerMoves.get(index);
      }
      return null;
    }

    @Override
    public Object last() {
      while (hasNext()) {
        next();
      }
      return current();
    }

    @Override
    public int currentIdx() {
      return index;
    }
  }
}
