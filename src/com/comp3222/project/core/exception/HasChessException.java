package com.comp3222.project.core.exception;

import com.comp3222.project.core.board.Grid;

public class HasChessException extends Exception {
  public HasChessException(Grid grid) {
    super(
        String.format(
            "Grid %s has %s's %s chess, you cannot set chess in it.",
            grid, grid.getChess().getPlayer(), grid.getChess()));
  }
}
