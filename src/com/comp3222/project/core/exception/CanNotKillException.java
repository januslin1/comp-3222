package com.comp3222.project.core.exception;

import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.Player;

public class CanNotKillException extends Exception {
  public CanNotKillException(Player player, Chess chess, Chess targetChess) {
    super(String.format("%s's %s cannot kill %s.", player, chess, targetChess));
  }
}
