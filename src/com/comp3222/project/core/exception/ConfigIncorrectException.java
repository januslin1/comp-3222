package com.comp3222.project.core.exception;

public class ConfigIncorrectException extends Exception {
  public ConfigIncorrectException(String message) {
    super(message);
  }
}
