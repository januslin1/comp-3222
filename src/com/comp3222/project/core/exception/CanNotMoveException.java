package com.comp3222.project.core.exception;

public class CanNotMoveException extends Exception {
  public CanNotMoveException(String message) {
    super(message);
  }
}
