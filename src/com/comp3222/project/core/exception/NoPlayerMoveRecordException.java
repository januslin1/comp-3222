package com.comp3222.project.core.exception;

public class NoPlayerMoveRecordException extends Exception {
  public NoPlayerMoveRecordException(String msg) {
    super(msg);
  }
}
