package com.comp3222.project.core;

/** The follow method must be created in order to config the game. */
public interface Config {

  void gridRegistry();

  void boardRegistry();

  void chessRegistry();
}
