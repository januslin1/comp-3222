package com.comp3222.project.core;

import com.comp3222.project.core.board.Grid;

public class GridRegistryEntry {
  private String gridName;
  private Class<? extends Grid> gridClass;

  public GridRegistryEntry(String gridName, Class<? extends Grid> gridClass) {
    this.gridName = gridName;
    this.gridClass = gridClass;
  }

  public String getGridName() {
    return gridName;
  }

  public Class<? extends Grid> getGridClass() {
    return gridClass;
  }
}
