package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Elephant extends Chess {

  public Elephant(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 8;
  }

  @Override
  String setChessName() {
    return "Elephant";
  }

  @Override
  public boolean canKill(Chess chess) {
    if (chess instanceof Mouse) return false;
    if (this.getStrength() >= chess.getStrength()) return true;
    return false;
  }
}
