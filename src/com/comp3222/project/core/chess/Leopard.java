package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Leopard extends Chess {

  public Leopard(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 5;
  }

  @Override
  String setChessName() {
    return "Leopard";
  }
}
