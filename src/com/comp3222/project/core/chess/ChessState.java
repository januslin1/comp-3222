package com.comp3222.project.core.chess;

public enum ChessState {
  alive,
  died
}
