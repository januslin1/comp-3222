package com.comp3222.project.core.chess;

import com.comp3222.project.Resource;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.Player;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Optional;

public abstract class Chess implements Serializable {
  private int strength;
  private ChessState status;
  private String imagePath;
  private Player player;
  private String chessName;
  private GameWorld gameWorld;

  /**
   * Constructor.
   *
   * @param status
   * @param imagePath
   * @param player
   */
  public Chess(String imagePath, Player player, GameWorld gameWorld) {
    this.imagePath = imagePath;
    this.player = player;

    this.strength = setStrength();
    this.chessName = setChessName();
    this.gameWorld = gameWorld;

    // Default status is alive
    setStatus(ChessState.alive);
  }

  public InputStream getImagePath() {
    return Resource.getInstance().get(imagePath);
  }

  public ChessState getStatus() {
    return status;
  }

  /**
   * Set the status of this chess.
   *
   * @param chessStatus Chess status.
   */
  public void setStatus(ChessState chessStatus) {
    this.status = chessStatus;
  }

  /**
   * Set the strength of this chess.
   *
   * @return the strength value.
   */
  abstract int setStrength();

  /**
   * Set the chess name.
   *
   * @return the chess name value.
   */
  abstract String setChessName();

  /**
   * Kill the target chess if this chess can.
   *
   * @param chess target chess.
   */
  public void kill(Chess chess) {
    chess.setStatus(ChessState.died);
  }

  public Player getPlayer() {
    return player;
  }

  public Boolean isCurrentPlayer() {
    try {
      return getPlayer() == gameWorld.getCurrentPlayer();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Get the status of this chess.
   *
   * @return this chess strength.
   */
  protected int getStrength() {
    return this.strength;
  }

  /**
   * Check whther this chess can kill the target.
   *
   * @param chess target chess.
   * @return whether the target can be killed.
   */
  public boolean canKill(Chess chess) {
    return this.getStrength() >= chess.getStrength();
  };

  /**
   * Check whther this chess belongs to this player.
   *
   * @param player Player for check.
   * @return whther this chess belongs to this player.
   */
  public boolean isPlayer(Player player) {
    return this.player == player;
  }

  @Override
  public String toString() {
    return this.setChessName();
  }

  public String getChessId() {
    return ("" + this.setChessName().charAt(0) + getPlayer().toString().charAt(0)).toLowerCase();
  }

  Optional<Grid> belongsToGrid() {
    try {
      return gameWorld.getGameBoard().getGridByChess(this);
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
    return null;
  }

  public void moveToGrid(Grid grid) throws Exception {
    Grid originalGrid = belongsToGrid().get();
    originalGrid.moveToGrid(grid);
  }
}
