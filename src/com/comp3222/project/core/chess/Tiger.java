package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Tiger extends Chess {

  public Tiger(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 6;
  }

  @Override
  String setChessName() {
    return "Tiger";
  }
}
