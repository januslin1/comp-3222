package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Cat extends Chess {

  public Cat(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 2;
  }

  @Override
  String setChessName() {
    return "Cat";
  }
}
