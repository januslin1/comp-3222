package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Mouse extends Chess {

  public Mouse(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 1;
  }

  @Override
  String setChessName() {
    return "Mouse";
  }

  @Override
  public boolean canKill(Chess chess) {
    if (chess instanceof Elephant) return true;
    if (this.getStrength() >= chess.getStrength()) return true;
    return false;
  }
}
