package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

public class Wolf extends Chess {

  public Wolf(String imagePath, Player player, GameWorld gameWorld) {
    super(imagePath, player, gameWorld);
  }

  @Override
  int setStrength() {
    return 3;
  }

  @Override
  String setChessName() {
    return "Wolf";
  }
}
