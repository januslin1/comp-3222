package com.comp3222.project.core.chess;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;
import com.comp3222.project.core.Registry;

import java.util.ArrayList;

public class ChessFactory {
  public static ArrayList<Chess> makeChess(GameWorld gameWorld) {
    ArrayList<Chess> chessList = new ArrayList<>();
    Registry.getChessRegistry()
        .forEach(
            chessRegistryEntity -> {
              try {
                Class<? extends Chess> chessClass = chessRegistryEntity.getChess();
                chessList.add(
                    chessClass
                        .getConstructor(String.class, Player.class, GameWorld.class)
                        .newInstance(
                            chessRegistryEntity.getChessImageForPlayerA(),
                            gameWorld.getPlayer1(),
                            gameWorld));
                chessList.add(
                    chessClass
                        .getConstructor(String.class, Player.class, GameWorld.class)
                        .newInstance(
                            chessRegistryEntity.getChessImageForPlayerB(),
                            gameWorld.getPlayer2(),
                            gameWorld));
              } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
              }
            });

    return chessList;
  }
}
