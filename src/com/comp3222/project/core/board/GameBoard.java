package com.comp3222.project.core.board;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.utility.ConsoleTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

public class GameBoard implements Serializable {
  private ArrayList<Grid> grids;
  private GameWorld gameWorld;

  public GameBoard(GameWorld gameWorld) {
    this.gameWorld = gameWorld;
    this.grids = new ArrayList<>();
    grids = GameBoardFactory.makeBoard(this, gameWorld);
  }

  private static String columnName(int index) {
    StringBuilder s = new StringBuilder();
    while (index >= 26) {
      s.insert(0, (char) ('a' + index % 26));
      index = index / 26 - 1;
    }
    s.insert(0, (char) ('a' + index));
    return s.toString();
  }

  public Optional<Grid> getGridByChess(Chess chess) {
    return this.grids.stream().filter(grid -> grid.getChess() == chess).findFirst();
  }

  public Optional<Grid> getGridById(String id) {
    return grids.stream().filter(grid -> grid.getId().equals(id.toLowerCase())).findFirst();
  }

  public Optional<Grid> getTopLeftGrid() {
    return grids.stream().filter(grid -> grid.isTopLeftGrid()).findFirst();
  }

  @Override
  public String toString() {
    ArrayList<ArrayList<String>> content = new ArrayList<ArrayList<String>>();
    // Find TopLeft
    Grid rowLeftpointer = grids.stream().filter(grid -> grid.isTopLeftGrid()).findFirst().get();
    Grid rowpointer = grids.stream().filter(grid -> grid.isTopLeftGrid()).findFirst().get();
    Integer rowCount = 1;
    while (rowLeftpointer != null) {
      ArrayList<String> newRow = new ArrayList<>();
      newRow.add(rowCount.toString());
      while (rowpointer != null) {
        newRow.add(rowpointer.toString());
        rowpointer = rowpointer.getRight();
      }
      content.add(newRow);
      rowCount++;
      rowLeftpointer = rowLeftpointer.getBottom();
      rowpointer = rowLeftpointer;
    }

    ArrayList<String> headers = new ArrayList<>();
    for (int x = 0; x < content.get(0).size(); x++) {
      if (x != 0) {
        headers.add(columnName(x - 1));
      } else {
        headers.add("");
      }
    }
    ConsoleTable ct = new ConsoleTable(headers, content);

    return ct.getTable();
  }
}
