package com.comp3222.project.core.board;

import com.comp3222.project.Resource;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

import java.io.InputStream;

public class DenA extends Den {
  public DenA(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  @Override
  public String symbol() {
    return "D";
  }

  @Override
  public InputStream imagePath() {
    return Resource.getInstance().get("denA.png");
  }

  @Override
  public Player getPlayerBelongsTo() throws Exception {
    return GameWorld.getInstance().getPlayerMap().get('A');
  }
}
