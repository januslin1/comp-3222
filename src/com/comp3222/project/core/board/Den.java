package com.comp3222.project.core.board;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

import java.util.ArrayList;

abstract class Den extends Grid {

  Den(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  abstract Player getPlayerBelongsTo() throws Exception;

  @Override
  public ArrayList<Grid> getAvaliableGridsToMove() {
    // This player wins, no need to move.
    return new ArrayList<>();
  }
}
