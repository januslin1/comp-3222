package com.comp3222.project.core.board;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.GridRegistryEntry;
import com.comp3222.project.core.Registry;

import java.util.ArrayList;
import java.util.Optional;

public class GameBoardFactory {
  public static ArrayList<Grid> makeBoard(GameBoard gameBoard, GameWorld gameWorld) {
    ArrayList<Grid> grids = new ArrayList<>();
    Registry.getBoardRegistry()
        .stream()
        .forEach(
            boardRegistryEntity -> {
              try {
                Optional<GridRegistryEntry> gridEntity =
                    Registry.getGridClassByTypeName(boardRegistryEntity.getGridType());
                Class<? extends Grid> gridClass = gridEntity.get().getGridClass();
                grids.add(
                    gridClass
                        .getConstructor(String.class, Boolean.class, GameWorld.class)
                        .newInstance(
                            boardRegistryEntity.getGridId(),
                            boardRegistryEntity.isTopLeftGrid(),
                            gameWorld));
              } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
              }
            });

    Registry.getBoardRegistry()
        .stream()
        .forEach(
            boardRegistryEntity -> {
              // TODO: Should check
              if (boardRegistryEntity.getBottomGridId() != null) {
                grids
                    .stream()
                    .filter(grid -> grid.getId().equals(boardRegistryEntity.getGridId()))
                    .findFirst()
                    .get()
                    .setBottom(
                        grids
                            .stream()
                            .filter(
                                grid -> grid.getId().equals(boardRegistryEntity.getBottomGridId()))
                            .findFirst()
                            .get());

                grids
                    .stream()
                    .filter(grid -> grid.getId().equals(boardRegistryEntity.getBottomGridId()))
                    .findFirst()
                    .get()
                    .setTop(
                        grids
                            .stream()
                            .filter(grid -> grid.getId().equals(boardRegistryEntity.getGridId()))
                            .findFirst()
                            .get());
              }

              if (boardRegistryEntity.getRightGridId() != null) {
                grids
                    .stream()
                    .filter(grid -> grid.getId().equals(boardRegistryEntity.getGridId()))
                    .findFirst()
                    .get()
                    .setRight(
                        grids
                            .stream()
                            .filter(
                                grid -> grid.getId().equals(boardRegistryEntity.getRightGridId()))
                            .findFirst()
                            .get());

                grids
                    .stream()
                    .filter(grid -> grid.getId().equals(boardRegistryEntity.getRightGridId()))
                    .findFirst()
                    .get()
                    .setLeft(
                        grids
                            .stream()
                            .filter(grid -> grid.getId().equals(boardRegistryEntity.getGridId()))
                            .findFirst()
                            .get());
              }
            });
    return grids;
  }
}
