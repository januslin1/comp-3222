package com.comp3222.project.core.board;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.GameWorldState;
import com.comp3222.project.core.Registry;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.chess.ChessState;
import com.comp3222.project.core.exception.CanNotKillException;
import com.comp3222.project.core.exception.CanNotMoveException;
import com.comp3222.project.core.exception.HasChessException;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

public abstract class Grid implements Serializable {
  private String id;
  private Chess chess;
  private Grid left;
  private Grid right;
  private Grid top;
  private Grid bottom;
  private Boolean isTopLeftGrid;
  private GameWorld gameWorld;

  public Grid(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    this.id = id;
    this.chess = null;
    this.left = null;
    this.right = null;
    this.top = null;
    this.bottom = null;
    this.isTopLeftGrid = isTopLeftGrid;
    this.gameWorld = gameWorld;
  }

  public GameWorld getGameWorld() {
    return gameWorld;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Chess getChess() {
    return chess;
  }

  public void setChess(Chess chess) throws HasChessException {
    if (hasChess()) throw new HasChessException(this);
    else this.chess = chess;
  }

  public Grid getLeft() {
    return left;
  }

  public void setLeft(Grid left) {
    this.left = left;
  }

  public Grid getRight() {
    return right;
  }

  public void setRight(Grid right) {
    this.right = right;
  }

  public Grid getTop() {
    return top;
  }

  public void setTop(Grid top) {
    this.top = top;
  }

  public Grid getBottom() {
    return bottom;
  }

  public void setBottom(Grid bottom) {
    this.bottom = bottom;
  }

  public boolean hasChess() {
    return getChess() != null;
  }

  public abstract ArrayList<Grid> getAvaliableGridsToMove() throws Exception;

  public void removeChess() {
    this.chess = null;
  }

  @Override
  public String toString() {

    if (getChess() != null) {
      return getChess().getChessId();
    }

    return symbol();
  }

  public boolean isTopLeftGrid() {
    return this.isTopLeftGrid;
  }

  public String symbol() {
    return " ";
  }

  public abstract InputStream imagePath();

  public void moveToGrid(Grid grid) throws Exception {
    if (getGameWorld().getState() == GameWorldState.start) {
      // check whether alive
      if (getChess().getStatus() != ChessState.alive) {
        throw new CanNotMoveException("This chess is not alive");
      }

      try {
        // check is cuurent player
        if (GameWorld.getInstance().getCurrentPlayer() != getChess().getPlayer())
          throw new CanNotMoveException("This is not your chess!");

        // Can move?
        if (!getAvaliableGridsToMove().contains(grid))
          throw new CanNotMoveException(
              "You cannot move " + this.toString() + " to " + Registry.getGridTypeName(grid) + "!");

        // Whther is grid has chess?
        if (grid.hasChess()) {

          // target chess is our chess??
          if (grid.getChess().getPlayer() == this.getChess().getPlayer())
            throw new CanNotMoveException("You cannot kill yourself!");

          // target grid is our Trap
          if (grid instanceof Trap
              && ((Trap) grid).getPlayerBelongsTo() == getChess().getPlayer()) {
            gameWorld
                .getPlayerMoveRepository()
                .add(this, grid, this.chess, Optional.of(grid.getChess()));
            this.getChess().kill(grid.getChess());
            grid.removeChess();
            grid.setChess(getChess());
            removeChess();
          } else if (this.getChess().canKill(grid.getChess())) {
            // if not check whether can kill target chess
            gameWorld
                .getPlayerMoveRepository()
                .add(this, grid, this.chess, Optional.of(grid.getChess()));
            this.getChess().kill(grid.getChess());
            grid.removeChess();
            grid.setChess(getChess());
            removeChess();
          } else {
            throw new CanNotMoveException(
                getChess().toString() + " cannot kill " + grid.getChess().toString() + "!");
          }
        } else {
          gameWorld.getPlayerMoveRepository().add(this, grid, this.chess, Optional.empty());
          grid.setChess(this.getChess());
          removeChess();
        }
        getGameWorld().checkGameTerminate();
      } catch (CanNotKillException e) {
        throw new CanNotMoveException(e.getMessage());

      } catch (HasChessException e) {
        throw new CanNotMoveException(e.getMessage());
      } catch (Exception e) {
        throw new CanNotMoveException(e.getMessage());
      }
    } else {
      throw new CanNotMoveException("Game terminated.");
    }
  }
}
