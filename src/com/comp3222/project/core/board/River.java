package com.comp3222.project.core.board;

import com.comp3222.project.Resource;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.chess.Mouse;

import java.io.InputStream;
import java.util.ArrayList;

public class River extends Grid {

  public River(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  @Override
  public ArrayList<Grid> getAvaliableGridsToMove() {
    ArrayList<Grid> grids = new ArrayList<Grid>();

    // The Mouse is the only animal that is allowed to move onto a water square.
    if (getChess() != null && getChess() instanceof Mouse) {
      if (getTop() != null && (getTop() instanceof NormalGrid || getTop() instanceof River))
        grids.add(getTop());

      if (getBottom() != null
          && (getBottom() instanceof NormalGrid || getBottom() instanceof River))
        grids.add(getBottom());

      if (getLeft() != null && (getLeft() instanceof NormalGrid || getLeft() instanceof River))
        grids.add(getLeft());

      if (getRight() != null && (getRight() instanceof NormalGrid || getRight() instanceof River))
        grids.add(getRight());
    }
    return grids;
  }

  @Override
  public String symbol() {
    return "≈";
  }

  @Override
  public InputStream imagePath() {
    return Resource.getInstance().get("river.png");
  }
}
