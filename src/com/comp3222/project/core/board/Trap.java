package com.comp3222.project.core.board;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

import java.util.ArrayList;

abstract class Trap extends Grid {
  public Trap(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  @Override
  public ArrayList<Grid> getAvaliableGridsToMove() throws Exception {
    ArrayList<Grid> grids = new ArrayList<Grid>();

    // Normal Grid Case

    if (getTop() != null && (getTop() instanceof NormalGrid || getTop() instanceof Trap))
      grids.add(getTop());

    if (getBottom() != null && (getBottom() instanceof NormalGrid || getBottom() instanceof Trap))
      grids.add(getBottom());

    if (getLeft() != null && (getLeft() instanceof NormalGrid || getLeft() instanceof Trap))
      grids.add(getLeft());

    if (getRight() != null && (getRight() instanceof NormalGrid || getRight() instanceof Trap))
      grids.add(getRight());

    // Den Grid Case

    // A player may not move its own piece into its own den.
    if (getTop() != null
        && getTop() instanceof Den
        && ((Den) getTop()).getPlayerBelongsTo() != getChess().getPlayer()) grids.add(getTop());
    if (getBottom() != null
        && getBottom() instanceof Den
        && ((Den) getBottom()).getPlayerBelongsTo() != getChess().getPlayer())
      grids.add(getBottom());
    if (getLeft() != null
        && getLeft() instanceof Den
        && ((Den) getLeft()).getPlayerBelongsTo() != getChess().getPlayer()) grids.add(getLeft());
    if (getRight() != null
        && getRight() instanceof Den
        && ((Den) getRight()).getPlayerBelongsTo() != getChess().getPlayer()) grids.add(getRight());

    return grids;
  }

  abstract Player getPlayerBelongsTo() throws Exception;
}
