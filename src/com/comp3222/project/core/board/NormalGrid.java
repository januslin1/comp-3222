package com.comp3222.project.core.board;

import com.comp3222.project.Resource;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.chess.Lion;
import com.comp3222.project.core.chess.Mouse;
import com.comp3222.project.core.chess.Tiger;

import java.io.InputStream;
import java.util.ArrayList;

public class NormalGrid extends Grid {

  public NormalGrid(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  @Override
  public ArrayList<Grid> getAvaliableGridsToMove() throws Exception {
    ArrayList<Grid> grids = new ArrayList<Grid>();

    // Normal Grid Case

    if (getTop() != null && (getTop() instanceof NormalGrid || getTop() instanceof Trap))
      grids.add(getTop());

    if (getBottom() != null && (getBottom() instanceof NormalGrid || getBottom() instanceof Trap))
      grids.add(getBottom());

    if (getLeft() != null && (getLeft() instanceof NormalGrid || getLeft() instanceof Trap))
      grids.add(getLeft());

    if (getRight() != null && (getRight() instanceof NormalGrid || getRight() instanceof Trap))
      grids.add(getRight());

    // River Grid Case

    // Mouse Case

    // The Mouse is the only animal that is allowed to move onto a water square.

    if (getChess() != null && getChess() instanceof Mouse) {

      if (getTop() != null && getTop() instanceof River) grids.add(getTop());

      if (getBottom() != null && getBottom() instanceof River) grids.add(getBottom());

      if (getLeft() != null && getLeft() instanceof River) grids.add(getLeft());

      if (getRight() != null && getRight() instanceof River) grids.add(getRight());
    }

    // Lion and Tiger case

    // They move from a square on one edge of the river to the next non-water square on the other
    // side. Such a move is not allowed if there is a rat (whether friendly or enemy) on any of the
    // intervening water squares. The lion and tiger are allowed to capture enemy pieces by such
    // jumping moves.

    if (getChess() instanceof Lion || getChess() instanceof Tiger) {
      // Up
      if (getTop() != null && getTop() instanceof River) {
        if (getTop() != null && getTop() instanceof River) {
          Boolean hasMouse = false;
          Grid pointer = this;

          while (pointer.getTop() != null && pointer.getTop() instanceof River) {
            pointer = pointer.getTop();
            if (pointer.hasChess() && pointer.getChess() instanceof Mouse) {
              hasMouse = true;
              break;
            }
          }

          pointer = pointer.getTop();

          if (!hasMouse) grids.add(pointer);
        }
      }

      // Bottom
      if (getBottom() != null && getBottom() instanceof River) {
        if (getBottom() != null && getBottom() instanceof River) {
          Boolean hasMouse = false;
          Grid pointer = this;

          while (pointer.getBottom() != null && pointer.getBottom() instanceof River) {
            pointer = pointer.getBottom();
            if (pointer.hasChess() && pointer.getChess() instanceof Mouse) {
              hasMouse = true;
              break;
            }
          }

          pointer = pointer.getBottom();

          if (!hasMouse) grids.add(pointer);
        }
      }

      // Left
      if (getLeft() != null && getLeft() instanceof River) {
        if (getLeft() != null && getLeft() instanceof River) {
          Boolean hasMouse = false;
          Grid pointer = this;

          while (pointer.getLeft() != null && pointer.getLeft() instanceof River) {
            pointer = pointer.getLeft();
            if (pointer.hasChess() && pointer.getChess() instanceof Mouse) {
              hasMouse = true;
              break;
            }
          }

          pointer = pointer.getLeft();

          if (!hasMouse) grids.add(pointer);
        }
      }

      // Right
      if (getRight() != null && getRight() instanceof River) {
        if (getRight() != null && getRight() instanceof River) {
          Boolean hasMouse = false;
          Grid pointer = this;

          while (pointer.getRight() != null && pointer.getRight() instanceof River) {
            pointer = pointer.getRight();
            if (pointer.hasChess() && pointer.getChess() instanceof Mouse) {
              hasMouse = true;
              break;
            }
          }

          pointer = pointer.getRight();

          if (!hasMouse) grids.add(pointer);
        }
      }
    }

    return grids;
  }

  @Override
  public InputStream imagePath() {
    return Resource.getInstance().get("grid.png");
  }
}
