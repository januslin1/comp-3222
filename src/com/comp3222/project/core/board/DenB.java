package com.comp3222.project.core.board;

import com.comp3222.project.Resource;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;

import java.io.InputStream;

public class DenB extends Den {
  public DenB(String id, Boolean isTopLeftGrid, GameWorld gameWorld) {
    super(id, isTopLeftGrid, gameWorld);
  }

  @Override
  public String symbol() {
    return "D";
  }

  @Override
  public InputStream imagePath() {
    return Resource.getInstance().get("denB.png");
  }

  @Override
  public Player getPlayerBelongsTo() throws Exception {
    return GameWorld.getInstance().getPlayerMap().get('B');
  }
}
