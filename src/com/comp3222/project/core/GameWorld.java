package com.comp3222.project.core;

import com.comp3222.project.core.board.GameBoard;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.chess.ChessFactory;
import com.comp3222.project.core.chess.ChessState;
import com.comp3222.project.core.exception.NoPlayerMoveRecordException;
import com.comp3222.project.core.utility.Iterator;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class GameWorld implements Serializable {
  private static GameWorld instance;
  private static Random rand = new Random();
  private GameBoard gameBoard;
  private List<Chess> chessList;
  private Player player1;
  private Player player2;
  private Integer round;
  private Character currentPlayer; // A or B
  private Map<Character, Player> playerMap; // A/B -> Player
  private Player winner;
  private GameWorldState state;
  private PlayerMoveRepository playerMoveRepository = new PlayerMoveRepository();

  public Player getPlayer1() {
    return player1;
  }

  public Player getPlayer2() {
    return player2;
  }

  private GameWorld(Player player1, Player player2, Boolean randomPlayerSequance) {
    currentPlayer = 'A';
    round = 1;
    setlayerInfo(player1, player2);
    initGameChess();

    if (randomPlayerSequance) {
      randomPlayerSequance();
    } else {
      defaultPlayerSequance();
    }

    resetGameBoard();
    setState(GameWorldState.start);
    System.out.println("Hello!");
    System.out.println(playerMap.get('A').getName() + " is the first player!");
  }

  public void undoMove() throws Exception {
    if (getPlayerMoveRepository().getSize() > 0) {
      Iterator playerMoveRepository = getPlayerMoveRepository().getIterator();
      PlayerMove undoAction = (PlayerMove) playerMoveRepository.last();
      if (undoAction != null) {
        if (playerMoveRepository.currentIdx() > 0) {
          getPlayerMoveRepository().subList(0, playerMoveRepository.currentIdx());
        } else {
          getPlayerMoveRepository().clearAll();
        }
        undoAction.fromGrid.setChess(undoAction.movingChess);
        undoAction.toGrid.removeChess();

        if (undoAction.deadChess.isPresent()) {
          Chess deadChess = undoAction.deadChess.get();
          undoAction.toGrid.setChess(deadChess);
          deadChess.setStatus(ChessState.alive);
        }

        if (getState() == GameWorldState.start) {
          if (currentPlayer == 'A') {
            currentPlayer = 'B';
            round--;
          } else {
            currentPlayer = 'A';
          }

          System.out.println("Now Term: " + getCurrentPlayer().getName() + " !");
        }
      }
    } else {
      throw new NoPlayerMoveRecordException("No previous player move record!");
    }
  }

  public PlayerMoveRepository getPlayerMoveRepository() {
    return playerMoveRepository;
  }

  public static GameWorld setInstance(
      Player player1, Player player2, Boolean randomPlayerSequance) {
    if (instance == null) {
      instance = new GameWorld(player1, player2, randomPlayerSequance);
    }
    return instance;
  }

  public static GameWorld getInstance() throws Exception {
    if (instance == null) throw new Exception("You have not create game instance!");
    return instance;
  }

  public static boolean hasInstance() {
    if (instance != null) return true;
    return false;
  }

  public static void loadGame() throws IOException, ClassNotFoundException {
    try {
      if (!new File("gameSave.data").exists()) throw new IOException("File not found!");
      FileInputStream fis = new FileInputStream("gameSave.data");
      ObjectInputStream ois = new ObjectInputStream(fis);
      instance = (GameWorld) ois.readObject();
      ois.close();
    } catch (Exception e) {
      throw e;
    }
  };

  public Player getWinner() {
    return winner;
  }

  public Integer getRound() {
    return round;
  }

  public GameBoard getGameBoard() {
    return gameBoard;
  }

  public List<Chess> getChessList() {
    return chessList;
  }

  private void randomPlayerSequance() {
    // Random assign player as A or B
    if (rand.nextInt(2) >= 2) {
      playerMap = new HashMap<>();
      playerMap.put('A', player1);
      playerMap.put('B', player2);
    } else {
      playerMap = new HashMap<>();
      playerMap.put('B', player1);
      playerMap.put('A', player2);
    }
  }

  private void defaultPlayerSequance() {
    // Player A is Player 1, Player B is Player 2
    playerMap = new HashMap<>();
    playerMap.put('A', player1);
    playerMap.put('B', player2);
  }

  public void changePlayerInfoAndGameReset(
      Player player1, Player player2, Boolean randomPlayerSequance) {
    setlayerInfo(player1, player2);
    if (randomPlayerSequance) {
      randomPlayerSequance();
    } else {
      defaultPlayerSequance();
    }
    initGameChess();
    gameReset();
  }

  private void setlayerInfo(Player player1, Player player2) {
    this.player1 = player1;
    this.player2 = player2;
  }

  public void gameReset() {
    currentPlayer = 'A';
    round = 1;
    setState(GameWorldState.start);
    winner = null;
    resetGameBoard();
    playerMoveRepository.clearAll();
    System.out.println(playerMap.get('A').getName() + " is the first player!");
  }

  public Player getCurrentPlayer() {
    return playerMap.get(currentPlayer);
  }

  public Optional<Chess> getChess(String id) {
    return this.chessList.stream().filter(chess -> chess.getChessId().equals(id)).findFirst();
  }

  public Optional<Chess> getCurrentPlayerChess(String id) {
    return this.chessList
        .stream()
        .filter(chess -> chess.getPlayer() == getCurrentPlayer() && chess.getChessId().equals(id))
        .findFirst();
  }

  public Player getPlayerByName(String name) {
    if (player1.toString().equals(name)) {
      return player1;
    } else if (player2.toString().equals(name)) {
      return player2;
    }
    return null;
  }

  private void resetGameBoard() {
    resetGameChess();
    gameBoard = new GameBoard(this);
    resetGameChessLocation();
    System.out.println(getGameBoard());
  }

  public Map<Character, Player> getPlayerMap() {
    return playerMap;
  }

  private void resetGameChessLocation() {
    Registry.getChessRegistry()
        .forEach(
            chessRegistryEntry -> {
              try {
                // Get animal chess
                List<Chess> chess =
                    chessList
                        .stream()
                        .filter(chess1 -> chess1.getClass() == chessRegistryEntry.getChess())
                        .collect(Collectors.toList());

                // Set PlayerA position
                Chess chessA =
                    chess
                        .stream()
                        .filter(chess1 -> chess1.getPlayer() == playerMap.get('A'))
                        .findFirst()
                        .get();

                System.out.println("");
                getGameBoard()
                    .getGridById(chessRegistryEntry.getPositionIdForPlayerA())
                    .get()
                    .setChess(chessA);

                // Set PlayerB position
                Chess chessB =
                    chess
                        .stream()
                        .filter(chess1 -> chess1.getPlayer() == playerMap.get('B'))
                        .findFirst()
                        .get();

                getGameBoard()
                    .getGridById(chessRegistryEntry.getPositionIdForPlayerB())
                    .get()
                    .setChess(chessB);
              } catch (Exception e) {
                e.printStackTrace();
                System.exit(1);
              }
            });
  }

  public void nextTern() {
    if (getState() == GameWorldState.start) {
      if (currentPlayer == 'A') {
        currentPlayer = 'B';
      } else {
        currentPlayer = 'A';
        round++;
      }

      System.out.println("Next Term: " + getCurrentPlayer().getName() + " !");
    }

    // todo: return exception
  }

  /** Reset all chess's status as alive. */
  private void resetGameChess() {
    this.chessList.forEach(chess -> chess.setStatus(ChessState.alive));
  }

  public List<Chess> getChessList(Player player) {
    return chessList
        .stream()
        .filter(chess -> chess.getPlayer() == player && chess.getStatus() == ChessState.alive)
        .collect(Collectors.toList());
  }

  private void initGameChess() {
    chessList = ChessFactory.makeChess(this);
  }

  public void saveGame() throws Exception {
    FileOutputStream fos = new FileOutputStream("gameSave.data");
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(this);
    oos.close();
  }

  private void setPlayerAWin() {
    winner = getPlayerMap().get('A');
    setState(GameWorldState.stop);
    System.out.println(String.format("%s win!", winner));
  }

  private void setPlayerBWin() {
    winner = getPlayerMap().get('B');
    setState(GameWorldState.stop);
    System.out.println(String.format("%s win!", winner));
  }

  public GameWorldState getState() {
    return state;
  }

  private void setState(GameWorldState state) {
    this.state = state;
  }

  public void checkGameTerminate() {
    // if Player chess all Killed
    if (getChessList(getPlayerMap().get('A')).size() == 0) {
      setPlayerBWin();
    }

    if (getChessList(getPlayerMap().get('B')).size() == 0) {
      setPlayerAWin();
    }

    // if Player's chess in Den
    Optional<Grid> optionalGrid = getGameBoard().getGridById("1D");
    if (optionalGrid.isPresent() && optionalGrid.get().hasChess()) {
      setPlayerBWin();
    }

    optionalGrid = getGameBoard().getGridById("9D");
    if (optionalGrid.isPresent() && optionalGrid.get().hasChess()) {
      setPlayerAWin();
    }
  }
}
