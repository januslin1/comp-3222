package com.comp3222.project.core.utility;

public interface Iterator {
  public boolean hasNext();

  public boolean hasPrevious();

  public Object next();

  public Object previous();

  public Object current();

  public Object last();

  public int currentIdx();
}
