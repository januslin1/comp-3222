package com.comp3222.project.core.utility;

public class Lang {
  public static String space(int length) {
    StringBuilder sb = new StringBuilder();
    space(sb, length);
    return sb.toString();
  }

  public static void space(StringBuilder sb, int length) {
    for (int i = 0; i < length; i++) {
      sb.append(' ');
    }
  }
}
