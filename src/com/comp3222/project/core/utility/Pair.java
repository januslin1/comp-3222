package com.comp3222.project.core.utility;

import java.util.function.Function;

public class Pair<A, B> {
  /* first */
  public final A fst;
  /* second */
  public final B snd;

  public Pair(A fst, B snd) {
    this.fst = fst;
    this.snd = snd;
  }

  public static <A, B> Pair<A, B> pair(A a, B b) {
    return new Pair<>(a, b);
  }

  public static <A, B> A fst(Pair<A, B> pair) {
    return pair.fst;
  }

  public static <A, B> B snd(Pair<A, B> pair) {
    return pair.snd;
  }

  @Override
  public String toString() {
    return "{" + fst + ',' + snd + '}';
  }

  public <C> Pair<C, B> map1(Function<A, C> f) {
    return pair(f.apply(fst), snd);
  }

  public <C> Pair<A, C> map2(Function<B, C> f) {
    return pair(fst, f.apply(snd));
  }
}
