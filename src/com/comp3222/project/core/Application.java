package com.comp3222.project.core;

import com.comp3222.project.cli.CommandLine;
import com.comp3222.project.core.exception.ConfigIncorrectException;
import com.comp3222.project.gui.GUI;

public abstract class Application implements Config {

  public Application() {
    try {
      gridRegistry();
      boardRegistry();
      chessRegistry();

      checkConfig();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(0);
    }
  }

  private void checkConfig() throws ConfigIncorrectException {
    if (Registry.getChessRegistry().size() <= 0)
      throw new ConfigIncorrectException("Chess Config Incorrect!");

    if (Registry.getGridRegistry().size() <= 0)
      throw new ConfigIncorrectException("Grid Config Incorrect!");

    if (Registry.getBoardRegistry().size() <= 0)
      throw new ConfigIncorrectException("Board Config Incorrect!");

    // Only one topLeft grid
    if (Registry.getBoardRegistry()
            .stream()
            .filter(boardRegistryEntry -> boardRegistryEntry.isTopLeftGrid())
            .count()
        != 1) throw new ConfigIncorrectException("Only one topleft grid is allowed!");
  }

  protected void startGUIMode() {
    System.out.println("Started gui mode.");
    new GUI().run();
  }

  protected void startCMLMode() {
    System.out.println("Started command line mode.");
    new CommandLine().run();
  }
}
