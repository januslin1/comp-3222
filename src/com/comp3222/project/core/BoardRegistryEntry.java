package com.comp3222.project.core;

public class BoardRegistryEntry {
  private String gridId;
  private String gridType;
  private String bottomGridId;
  private String rightGridId;
  private Boolean isTopLeftGrid;

  public BoardRegistryEntry(
      String gridId,
      String gridType,
      String bottomGridId,
      String rightGridId,
      boolean isTopLeftGrid) {
    this.gridId = gridId;
    this.gridType = gridType;
    this.bottomGridId = bottomGridId;
    this.rightGridId = rightGridId;
    this.isTopLeftGrid = isTopLeftGrid;
  }

  public Boolean isTopLeftGrid() {
    return isTopLeftGrid;
  }

  public String getGridId() {
    return gridId;
  }

  public String getGridType() {
    return gridType;
  }

  public String getBottomGridId() {
    return bottomGridId;
  }

  public String getRightGridId() {
    return rightGridId;
  }
}
