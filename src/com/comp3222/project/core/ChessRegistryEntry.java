package com.comp3222.project.core;

import com.comp3222.project.core.chess.Chess;

public class ChessRegistryEntry {
  private Class<? extends Chess> chessClass;
  private String positionIdForPlayerA;
  private String positionIdForPlayerB;
  private String chessImageForPlayerA;
  private String chessImageForPlayerB;

  public ChessRegistryEntry(
      Class<? extends Chess> chessClass,
      String positionIdForPlayerA,
      String positionIdForPlayerB,
      String chessImageForPlayerA,
      String chessImageForPlayerB) {
    this.chessClass = chessClass;
    this.positionIdForPlayerA = positionIdForPlayerA;
    this.positionIdForPlayerB = positionIdForPlayerB;
    this.chessImageForPlayerA = chessImageForPlayerA;
    this.chessImageForPlayerB = chessImageForPlayerB;
  }

  public String getChessImageForPlayerA() {
    return chessImageForPlayerA;
  }

  public String getChessImageForPlayerB() {
    return chessImageForPlayerB;
  }

  public Class<? extends Chess> getChess() {
    return chessClass;
  }

  public String getPositionIdForPlayerA() {
    return positionIdForPlayerA;
  }

  public String getPositionIdForPlayerB() {
    return positionIdForPlayerB;
  }
}
