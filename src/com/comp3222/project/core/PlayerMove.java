package com.comp3222.project.core;

import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;

import java.util.Optional;

public class PlayerMove {
  public final Grid fromGrid;
  public final Grid toGrid;
  public final Chess movingChess;
  public final Optional<Chess> deadChess;

  public PlayerMove(Grid fromGrid, Grid toGrid, Chess movingChess, Optional<Chess> deadChess) {
    this.fromGrid = fromGrid;
    this.toGrid = toGrid;
    this.movingChess = movingChess;
    this.deadChess = deadChess;
  }
}
