package com.comp3222.project.core;

import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;

import java.util.ArrayList;
import java.util.Optional;

public class Registry {
  private static ArrayList<ChessRegistryEntry> chessRegistry = new ArrayList<>();
  private static ArrayList<GridRegistryEntry> gridRegistry = new ArrayList<>();
  private static ArrayList<BoardRegistryEntry> boardRegistry = new ArrayList<>();

  public static void chessRegister(
      Class<? extends Chess> chessClass,
      String positionIdForPlayerA,
      String positionIdForPlayerB,
      String chessImageForPlayerA,
      String chessImageForPlayerB) {
    chessRegistry.add(
        new ChessRegistryEntry(
            chessClass,
            positionIdForPlayerA.toLowerCase(),
            positionIdForPlayerB.toLowerCase(),
            chessImageForPlayerA,
            chessImageForPlayerB));
  }

  public static void boardRegister(
      String gridId,
      String gridType,
      String bottomGridId,
      String rightGridId,
      boolean isTopLeftGrid) {
    if (!hasGridType(gridType)) throw new IllegalArgumentException("GridType not found.");
    boardRegistry.add(
        new BoardRegistryEntry(
            gridId.toLowerCase(),
            gridType.toLowerCase(),
            (bottomGridId == null) ? null : bottomGridId.toLowerCase(),
            (rightGridId == null) ? null : rightGridId.toLowerCase(),
            isTopLeftGrid));
  }

  public static void gridRegister(String gridName, Class<? extends Grid> gridClass) {
    gridRegistry.add(new GridRegistryEntry(gridName.toLowerCase(), gridClass));
  }

  private static boolean hasGridType(String gridType) {
    return gridRegistry
        .stream()
        .anyMatch(
            gridRegistryEntity -> gridRegistryEntity.getGridName().equals(gridType.toLowerCase()));
  }

  public static String getGridTypeName(Grid grid) {
    Optional<GridRegistryEntry> result =
        getGridRegistry()
            .stream()
            .filter(gridRegistryEntry -> gridRegistryEntry.getGridClass() == grid.getClass())
            .findFirst();
    if (result.isPresent()) return result.get().getGridName();
    return null;
  }

  public static Optional<GridRegistryEntry> getGridClassByTypeName(String typeName) {
    return gridRegistry
        .stream()
        .filter(
            gridRegistryEntity -> gridRegistryEntity.getGridName().equals(typeName.toLowerCase()))
        .findFirst();
  }

  public static ArrayList<ChessRegistryEntry> getChessRegistry() {
    return chessRegistry;
  }

  public static ArrayList<GridRegistryEntry> getGridRegistry() {
    return gridRegistry;
  }

  public static ArrayList<BoardRegistryEntry> getBoardRegistry() {
    return boardRegistry;
  }
}
