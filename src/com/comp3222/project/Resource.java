package com.comp3222.project;

import java.io.InputStream;

public class Resource {
  private static Resource instance;

  public static Resource getInstance() {
    if (instance == null) instance = new Resource();

    return instance;
  }

  public InputStream get(String filename) {
    return getClass().getResourceAsStream("resources/" + filename);
    // return System.getProperty("user.dir") + "/resources/" + filename;
  }
}
