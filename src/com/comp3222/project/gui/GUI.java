package com.comp3222.project.gui;

import com.comp3222.project.gui.mainWindow.MainWIndowFx;

public class GUI implements Runnable {

  @Override
  public void run() {
    MainWIndowFx.start();
  }
}
