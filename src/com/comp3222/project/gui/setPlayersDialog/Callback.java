package com.comp3222.project.gui.setPlayersDialog;

public interface Callback {
  void run(SetPlayersDialogFx.Results results);
}
