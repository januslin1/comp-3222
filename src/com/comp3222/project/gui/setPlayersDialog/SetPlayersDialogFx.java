package com.comp3222.project.gui.setPlayersDialog;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Optional;

public class SetPlayersDialogFx extends Application {
  private Callback callback;

  public SetPlayersDialogFx(Callback callback) {
    this.callback = callback;
  }

  @Override
  public void start(Stage primaryStage) {
    Dialog<Results> dialog = new Dialog<>();
    dialog.setTitle("Set New Players");
    dialog.setHeaderText("Please specify…");
    DialogPane dialogPane = dialog.getDialogPane();
    dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
    TextField textField = new TextField("Bob");
    TextField textField2 = new TextField("Alice");
    CheckBox cb1 = new CheckBox();
    textField.setPromptText("Player1 Name");
    textField2.setPromptText("Player2 Name");
    cb1.setText("Random");
    dialogPane.setContent(new VBox(8, textField, textField2, cb1));

    Platform.runLater(textField::requestFocus);
    dialog.setResultConverter(
        (ButtonType button) -> {
          if (button == ButtonType.OK) {
            return new Results(textField.getText(), textField2.getText(), cb1.isSelected());
          }
          return null;
        });
    Optional<Results> optionalResult = dialog.showAndWait();
    optionalResult.ifPresent(callback::run);
  }

  public static class Results {

    public String player1name;
    public String player2name;
    public Boolean isRandom;

    private Results(String player1name, String player2name, Boolean isRandom) {
      this.player1name = player1name;
      this.player2name = player2name;
      this.isRandom = isRandom;
    }
  }
}
