package com.comp3222.project.gui.dialog;

class Dialog {
  private String content;
  private String title;
  private String header;
  private javafx.scene.control.Alert.AlertType alertType;

  Dialog(
      String content, String title, String header, javafx.scene.control.Alert.AlertType alertType) {
    this.content = content;
    this.title = title;
    this.header = header;
    this.alertType = alertType;
    steup();
  }

  private void steup() {
    javafx.scene.control.Alert alert = new javafx.scene.control.Alert(alertType);
    alert.setTitle(title);
    alert.setHeaderText(header);
    alert.setContentText(content);

    alert.showAndWait();
  }
}
