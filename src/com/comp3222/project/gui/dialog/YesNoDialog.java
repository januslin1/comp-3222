package com.comp3222.project.gui.dialog;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class YesNoDialog {
  YesNoDialog(String content, Callback yesCallback, Callback noCallback) {

    ButtonType yes = new ButtonType("Yes", ButtonBar.ButtonData.OK_DONE);
    ButtonType no = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, content, yes, no);

    alert.setTitle("Confirmation Dialog");
    Optional<ButtonType> result = alert.showAndWait();

    if (result.isPresent() && result.get() == yes) {
      yesCallback.run();
    }

    if (result.isPresent() && result.get() == no) {
      noCallback.run();
    }
  }

  public static interface Callback {
    void run();
  }
}
