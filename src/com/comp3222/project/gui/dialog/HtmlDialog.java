package com.comp3222.project.gui.dialog;

import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;

public class HtmlDialog {
  private String title;
  private String content;

  HtmlDialog(String title, String content) {
    this.title = title;
    this.content = content;
    show();
  }

  private void show() {
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(title);
    WebView webView = new WebView();
    webView.getEngine().loadContent(content);
    webView.setContextMenuEnabled(false);
    webView.setPrefSize(500, 200);
    GridPane expContent = new GridPane();
    expContent.setMaxWidth(Double.MAX_VALUE);
    expContent.add(webView, 0, 1);
    alert.getDialogPane().setContent(expContent);
    alert.showAndWait();
  }
}
