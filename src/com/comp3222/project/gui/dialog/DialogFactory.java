package com.comp3222.project.gui.dialog;

import javafx.scene.control.Alert;

public class DialogFactory {
  public static Dialog getInformationDialog(String header, String content) {
    return new Dialog(content, "Information Dialog", header, Alert.AlertType.INFORMATION);
  }

  public static Dialog getWarningDialog(String header, String content) {
    return new Dialog(content, "Warning Dialog", header, Alert.AlertType.WARNING);
  }

  public static Dialog getErrorDialog(String header, String content) {
    return new Dialog(content, "Error Dialog", header, Alert.AlertType.ERROR);
  }

  public static void getYesNoDialog(
      String content, YesNoDialog.Callback okCallback, YesNoDialog.Callback cancelCallback) {
    new YesNoDialog(content, okCallback, cancelCallback);
  }

  public static void getExceptionDialog(Exception ex) {
    new ExceptionDialog(ex);
  }

  public static void getHtmlDialog(String title, String content) {
    new HtmlDialog(title, content);
  }
}
