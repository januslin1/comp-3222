package com.comp3222.project.gui.mainWindow;

import com.comp3222.project.core.chess.ChessState;
import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.GameWorldState;
import com.comp3222.project.gui.dialog.DialogFactory;
import com.comp3222.project.gui.mainWindow.Canvas.BoardGrid;
import com.comp3222.project.gui.mainWindow.Canvas.ChessEntity;
import com.comp3222.project.gui.mainWindow.Canvas.Entity;
import com.comp3222.project.gui.mainWindow.Canvas.EntityManager;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

import java.util.*;

public class Screen extends Thread {
  private static Screen instance;
  private int frame = 0;
  private Map<Integer, Integer> frameCallbacks = new HashMap<>();
  private GraphicsContext gc;
  private Canvas canvas;
  private boolean started = false;
  private int DROP_LEFT = 0;

  private Screen(GraphicsContext gc, Canvas canvas) {
    super();
    this.gc = gc;
    this.canvas = canvas;
  }

  public static Screen getInstance(GraphicsContext gc, Canvas canvas) {
    if (instance == null) instance = new Screen(gc, canvas);
    return instance;
  }

  public static Screen getInstance() {
    return instance;
  }

  @Override
  public synchronized void start() {
    if (!started) {
      started = true;
      super.start();
    }
    ;
  }

  public int getFrame() {
    return frame;
  }

  public void run() {
    System.gc();

    MoveState moveState = new MoveState();

    // Move Event
    canvas.setOnMouseMoved(
        event -> {
          try {
            if (GameWorld.getInstance().getState() == GameWorldState.start) {
              EntityManager.getInstance()
                  .getAllHighlightedChessEntities()
                  .forEach(entity -> ((ChessEntity) entity).setHighlight(false));
              Optional<Entity> entity =
                  EntityManager.getInstance().getChessByXY(event.getX(), event.getY());

              if (entity.isPresent()) {
                ChessEntity chessEntity = (ChessEntity) entity.get();
                if (chessEntity.getChess().getStatus() == ChessState.alive)
                  chessEntity.setHighlight(true);
              }
            }
          } catch (Exception e) {
            e.printStackTrace();
            DialogFactory.getExceptionDialog(e);
          }
        });

    canvas.setOnMousePressed(
        event -> {
          try {
            if (GameWorld.getInstance().getState() == GameWorldState.start) {
              Optional<Entity> entity =
                  EntityManager.getInstance().getChessByXY(event.getX(), event.getY());
              if (entity.isPresent()) {
                ChessEntity chessEntity = (ChessEntity) entity.get();

                if (chessEntity.getChess().getStatus() == ChessState.alive) {
                  moveState.selectedChess = chessEntity;
                  moveState.selectedChess.dragStart(
                      event.getX() - (moveState.selectedChess.getW() / 2),
                      event.getY() - (moveState.selectedChess.getH() / 2));

                  // Find Avaliable grid for move
                  GameWorld.getInstance()
                      .getGameBoard()
                      .getGridByChess(chessEntity.getChess())
                      .get()
                      .getAvaliableGridsToMove()
                      .forEach(
                          grid -> {
                            BoardGrid boardGrid =
                                ((BoardGrid)
                                    EntityManager.getInstance().getGridEntity(grid.getId()).get());
                            moveState.highlightedGrid.add(boardGrid);
                            boardGrid.setHighlight(true);
                          });
                }
              }
            }
          } catch (Exception e) {
            e.printStackTrace();
            DialogFactory.getExceptionDialog(e);
          }
        });

    canvas.setOnMouseDragged(
        event -> {
          if (moveState.selectedChess != null) {
            moveState.selectedChess.dragMove(
                event.getX() - (moveState.selectedChess.getW() / 2),
                event.getY() - (moveState.selectedChess.getH() / 2));
          }
        });

    canvas.setOnMouseReleased(
        event -> {
          try {
            if (moveState.selectedChess != null) {
              Optional<Entity> entity =
                  EntityManager.getInstance().getBoardGridByXY(event.getX(), event.getY());
              if (entity.isPresent()) {
                BoardGrid boardGrid = (BoardGrid) entity.get();

                if (moveState.highlightedGrid.contains(boardGrid)) {
                  moveState.selectedChess.getChess().moveToGrid(boardGrid.getGrid());
                  moveState.selectedChess.dragEnd(boardGrid.getX(), boardGrid.getY());
                  GameWorld.getInstance().nextTern();
                  moveState.selectedChess = null;
                } else {
                  moveState.selectedChess.dragEnd();
                  moveState.selectedChess = null;
                }
              }
            }

            if (moveState.highlightedGrid.size() > 0) {
              moveState.highlightedGrid.forEach(boardGrid -> boardGrid.setHighlight(false));
              moveState.highlightedGrid.clear();
            }

            // has Winner??
            if (GameWorld.getInstance().getWinner() != null) {
              DialogFactory.getInformationDialog(
                  "Info", String.format("%s win!", GameWorld.getInstance().getWinner()));
            }
          } catch (Exception e) {
            DialogFactory.getWarningDialog("Info", e.getMessage());
            moveState.selectedChess.dragEnd();
          }
        });

    // Start Screen
    new AnimationTimer() {
      @Override
      public void handle(long currentNanoTime) {
        if (DROP_LEFT > 0) {
          DROP_LEFT--;
        } else {
          nextFrame();
          render();
        }
      }
    }.start();
  }

  private void cls() {
    gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
  }

  private void render() {
    cls();
    EntityManager.getInstance().reDrawWholeScreen();
  }

  private void nextFrame() {
    this.frame++;
  }

  private static class MoveState {
    private ChessEntity selectedChess;
    private List<BoardGrid> highlightedGrid = new ArrayList<>();
  }
}
