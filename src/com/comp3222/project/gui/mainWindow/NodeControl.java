package com.comp3222.project.gui.mainWindow;

import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;

import java.util.Map;

abstract class NodeControl {
  @FXML private Node object;
  @FXML private Stage stage;

  NodeControl(Map<String, NodeControl> nodes, Stage stage, String nodeId) {
    this.object = stage.getScene().lookup(nodeId);
    this.stage = stage;
    setup(nodes, stage, object);
  }

  abstract void setup(Map<String, NodeControl> nodes, Stage stage, Node object);
}
