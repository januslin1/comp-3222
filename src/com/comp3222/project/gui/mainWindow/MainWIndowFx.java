package com.comp3222.project.gui.mainWindow;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.HashMap;
import java.util.Map;

public class MainWIndowFx extends Application {

  private Map<String, NodeControl> nodes = new HashMap<>();

  public static void start() {
    Application.launch();
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Parent root = FXMLLoader.load(getClass().getResource("MainWindowLayout.fxml"));
    Scene scene = new Scene(root);

    primaryStage.setTitle("Jungle Game");
    primaryStage.setScene(scene);
    primaryStage.setResizable(false);
    primaryStage.show();

    //
    nodes.put("#canvas1", new CanvasControl(nodes, primaryStage, "#canvas1"));
    nodes.put("#menubar1", new MenuBarControl(nodes, primaryStage, "#menubar1"));
  }
}
