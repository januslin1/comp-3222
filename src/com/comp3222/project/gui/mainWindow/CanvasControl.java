package com.comp3222.project.gui.mainWindow;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.gui.dialog.DialogFactory;
import com.comp3222.project.gui.mainWindow.Canvas.*;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.Map;
import java.util.Optional;

public class CanvasControl extends NodeControl {
  private GraphicsContext gc;
  private Canvas canvas;
  private int gridLength = 60;

  CanvasControl(Map<String, NodeControl> nodes, Stage stage, String nodeId) {
    super(nodes, stage, nodeId);
  }

  @Override
  void setup(Map<String, NodeControl> nodes, Stage stage, Node object) {
    gc = ((Canvas) object).getGraphicsContext2D();
    canvas = ((Canvas) object);
    gc.setFill(Color.BLACK);
    gc.setLineWidth(2);
    Font theFont = Font.font("Arial", FontWeight.BOLD, 30);
    gc.setFont(theFont);
    gc.fillText("Game not yet start!", 60, 50);
  }

  public void initGameScreen() {
    EntityManager.getInstance(gridLength, gc, canvas, false);
    EntityManager.getInstance().resetAll();
    setCurrentPlayerSegment();
    setCurrentRoundSegment();
    setBoardSegment();
    setChessSegment();

    EntityManager.getInstance()
        .add(new TimeFrameEntity(point(4), point(0.3), point(1), point(1), 2, gc));

    start();
  }

  public void setCurrentPlayerSegment() {
    try {
      EntityManager.getInstance()
          .add(new CurrentPlayerSegment(point(0.5), point(0.75), point(3), point(1), 1, gc));
    } catch (Exception ex) {
      DialogFactory.getExceptionDialog(ex);
    }
  }

  public void setCurrentRoundSegment() {
    try {
      EntityManager.getInstance()
          .add(new CurrentRoundSegment(point(5), point(0.75), point(2), point(1), 1, gc));
    } catch (Exception ex) {
      DialogFactory.getExceptionDialog(ex);
    }
  }

  private void start() {
    Screen.getInstance(gc, canvas).start();
  }

  private void stop() {
    Screen.getInstance(gc, canvas).stop();
  }

  public void setChessSegment() {
    try {
      GameWorld.getInstance()
          .getChessList()
          .forEach(
              chess -> {
                try {
                  Optional<Grid> grid =
                      GameWorld.getInstance().getGameBoard().getGridByChess(chess);

                  if (grid.isPresent()) {
                    Optional<Entity> entity =
                        EntityManager.getInstance().getGridEntity(grid.get().getId());
                    BoardGrid boardGrid = (BoardGrid) entity.get();
                    EntityManager.getInstance()
                        .add(
                            new ChessEntity(
                                boardGrid.getX(),
                                boardGrid.getY(),
                                point(1),
                                point(1),
                                3,
                                gc,
                                chess));
                  }

                } catch (Exception e) {
                  e.printStackTrace();
                  DialogFactory.getExceptionDialog(e);
                }
              });
    } catch (Exception e) {
      e.printStackTrace();
      DialogFactory.getExceptionDialog(e);
    }
  }

  public void setBoardSegment() {
    try {
      Grid pointer = GameWorld.getInstance().getGameBoard().getTopLeftGrid().get();
      Grid rowpLeft = GameWorld.getInstance().getGameBoard().getTopLeftGrid().get();
      double topLeftX = point(0);
      double topLeftY = point(1);

      int countX = 1;
      int countY = 1;

      while (rowpLeft != null) {
        while (pointer != null) {
          EntityManager.getInstance()
              .add(
                  new BoardGrid(
                      topLeftX + point(countX - 1),
                      topLeftY + (point(countY - 1)),
                      point(1),
                      point(1),
                      1,
                      gc,
                      pointer));
          pointer = pointer.getRight();
          countX++;
        }
        pointer = rowpLeft.getBottom();
        rowpLeft = pointer;
        countX = 1;
        countY++;
      }

    } catch (Exception ex) {
      ex.printStackTrace();
      DialogFactory.getExceptionDialog(ex);
    }
  }

  private double point(double x) {
    return EntityManager.getInstance().point(x);
  }
}
