package com.comp3222.project.gui.mainWindow;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.Player;
import com.comp3222.project.core.exception.NoPlayerMoveRecordException;
import com.comp3222.project.gui.dialog.DialogFactory;
import com.comp3222.project.gui.dialog.YesNoDialog;
import com.comp3222.project.gui.setPlayersDialog.Callback;
import com.comp3222.project.gui.setPlayersDialog.SetPlayersDialogFx;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.util.Map;

class MenuBarControl extends NodeControl {
  MenuBarControl(Map<String, NodeControl> nodes, Stage stage, String nodeId) {
    super(nodes, stage, nodeId);
  }

  @Override
  void setup(Map<String, NodeControl> nodes, Stage stage, Node object) {
    for (final Menu menu : ((MenuBar) object).getMenus()) {
      for (final MenuItem item : menu.getItems()) {
        final String itemId = item.getId();

        item.setOnAction(
            new EventHandler<ActionEvent>() {
              @Override
              public void handle(ActionEvent event) {
                menuAction(nodes, stage, itemId);
              }
            });
      }
    }
  }

  private void menuAction(Map<String, NodeControl> nodes, Stage stage, String itemId) {
    System.out.println(itemId);
    switch (itemId) {
      case "newBtn":
        if (!GameWorld.hasInstance()) {
          new SetPlayersDialogFx(
                  new Callback() {
                    @Override
                    public void run(SetPlayersDialogFx.Results results) {

                      GameWorld.setInstance(
                          new Player(results.player1name),
                          new Player(results.player2name),
                          results.isRandom);
                    }
                  })
              .start(stage);
        } else {
          DialogFactory.getYesNoDialog(
              "Do you need to set new players?",
              new YesNoDialog.Callback() {
                @Override
                public void run() {
                  new SetPlayersDialogFx(
                          new Callback() {
                            @Override
                            public void run(SetPlayersDialogFx.Results results) {

                              try {
                                GameWorld.getInstance()
                                    .changePlayerInfoAndGameReset(
                                        new Player(results.player1name),
                                        new Player(results.player2name),
                                        results.isRandom);
                              } catch (Exception e) {
                                DialogFactory.getExceptionDialog(e);
                              }
                            }
                          })
                      .start(stage);
                }
              },
              new YesNoDialog.Callback() {
                @Override
                public void run() {
                  try {
                    GameWorld.getInstance().gameReset();
                  } catch (Exception e) {
                    DialogFactory.getExceptionDialog(e);
                  }
                }
              });
        }
        if (GameWorld.hasInstance()) {
          ((CanvasControl) nodes.get("#canvas1")).initGameScreen();
        }
        break;
      case "saveBtn":
        try {
          GameWorld.getInstance().saveGame();
          DialogFactory.getInformationDialog("Information", "You have saved game successfully!");
        } catch (Exception e) {
          DialogFactory.getExceptionDialog(e);
        }
        break;

      case "loadBtn":
        try {
          GameWorld.loadGame();
          DialogFactory.getInformationDialog(
              "Information", "You have loaded saved game successfully!");

          if (GameWorld.hasInstance()) {
            ((CanvasControl) nodes.get("#canvas1")).initGameScreen();
          }

        } catch (Exception e) {
          DialogFactory.getExceptionDialog(e);
        }
        break;

      case "aboutBtn":
        DialogFactory.getHtmlDialog(
            "About this application",
            "<p>This application is for the group assignment of COMP3222 course from HKPolyU. Traditional Chinese board game - Jungle Game is the main theme of this project.</p> <h3>Author</h3><p>Group 15</p><ul><li>Lin Ho Ching Janus</li></ul>");
        break;

      case "undoBtn":
        try {
          GameWorld.getInstance().undoMove();
        } catch (NoPlayerMoveRecordException e) {
          DialogFactory.getWarningDialog("Warning", e.getMessage());
        } catch (Exception e) {
          DialogFactory.getExceptionDialog(e);
        }
        break;
    }
  }
}
