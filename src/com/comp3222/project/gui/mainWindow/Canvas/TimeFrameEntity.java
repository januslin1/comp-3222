package com.comp3222.project.gui.mainWindow.Canvas;

import com.comp3222.project.gui.mainWindow.Screen;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class TimeFrameEntity extends Entity {
  public TimeFrameEntity(double x, double y, double w, double h, int layer, GraphicsContext gc) {
    super(x, y, w, h, layer, gc);
  }

  @Override
  public void draw() throws Exception {
    Font theFont = Font.font("Arial", FontWeight.BOLD, 20);
    getGc().setFont(theFont);
    getGc().fillText("Frame: " + Screen.getInstance().getFrame(), getX(), getY());
  }
}
