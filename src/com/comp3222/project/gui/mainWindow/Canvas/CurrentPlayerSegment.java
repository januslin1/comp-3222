package com.comp3222.project.gui.mainWindow.Canvas;

import com.comp3222.project.core.GameWorld;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class CurrentPlayerSegment extends Entity {
  public CurrentPlayerSegment(
      double x, double y, double w, double h, int layer, GraphicsContext gc) {
    super(x, y, w, h, layer, gc);
  }

  @Override
  public void draw() throws Exception {
    Font theFont = Font.font("Arial", FontWeight.BOLD, 20);
    getGc().setFont(theFont);
    getGc()
        .fillText(
            "Current: " + GameWorld.getInstance().getCurrentPlayer().toString(), getX(), getY());
  }
}
