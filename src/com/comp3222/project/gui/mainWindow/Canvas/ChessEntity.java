package com.comp3222.project.gui.mainWindow.Canvas;

import com.comp3222.project.core.GameWorld;
import com.comp3222.project.core.board.Grid;
import com.comp3222.project.core.chess.Chess;
import com.comp3222.project.core.chess.ChessState;
import com.comp3222.project.gui.dialog.DialogFactory;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.Optional;

public class ChessEntity extends Entity {
  private Image image;
  private Chess chess;
  private Boolean highlight = false;
  private ChessEntityStatus chessEntityStatus = ChessEntityStatus.Standing;
  private Double movingX = 0.0;
  private Double movingY = 0.0;
  private int orignalLayer = 0;

  public ChessEntity(
      double x, double y, double w, double h, int layer, GraphicsContext gc, Chess chess)
      throws Exception {
    super(x, y, w, h, layer, gc);
    this.chess = chess;

    // File file = new File(chess.getImagePath().toURI());
    // FileInputStream input = new FileInputStream(file);
    image = new Image(chess.getImagePath());
    this.chess = chess;
  }

  public Boolean getHighlight() {
    return highlight;
  }

  public void setHighlight(Boolean highlight) {
    this.highlight = highlight;
  }

  public ChessEntityStatus getChessEntityStatus() {
    return chessEntityStatus;
  }

  public Chess getChess() {
    return chess;
  }

  public void dragStart(Double x, Double y) {
    orignalLayer = getLayer();
    setLayer(orignalLayer + 1);
    EntityManager.getInstance().sort();
    dragMove(x, y);
  }

  public void dragMove(Double x, Double y) {
    movingX = x;
    movingY = y;
    chessEntityStatus = ChessEntityStatus.Moving;
  }

  public void dragEnd(Double x, Double y) {
    setX(x);
    setY(y);
    dragEnd();
  }

  public void dragEnd() {
    setLayer(orignalLayer);
    EntityManager.getInstance().sort();
    chessEntityStatus = ChessEntityStatus.Standing;
  }

  @Override
  public void draw() {
    try {
      if (highlight && chessEntityStatus == ChessEntityStatus.Standing) {
        highlight();
      }

      if (chess.getStatus() == ChessState.alive) {
        if (chessEntityStatus == ChessEntityStatus.Moving) {
          getGc().drawImage(image, movingX, movingY, getW(), getH());
        } else {
          // Get Grid pos by Grid Id
          Optional<Grid> optionalGrid =
              GameWorld.getInstance().getGameBoard().getGridByChess(chess);
          if (optionalGrid.isPresent()) {
            Optional<Entity> optionalBoardGrid =
                EntityManager.getInstance().getGridEntity(optionalGrid.get().getId());
            if (optionalBoardGrid.isPresent()) {
              setX(optionalBoardGrid.get().getX());
              setY(optionalBoardGrid.get().getY());
            }
          }
          getGc().drawImage(image, getX(), getY(), getW(), getH());
        }
      }
    } catch (Exception e) {
      DialogFactory.getExceptionDialog(e);
      System.exit(0);
    }
  }

  private void highlight() {
    getGc().setGlobalAlpha(0.3);
    getGc().setFill(Color.GRAY);
    getGc().fillRect(getX(), getY(), getW(), getH());
    getGc().setGlobalAlpha(1);
    getGc().setFill(Color.BLACK);
  }
}
