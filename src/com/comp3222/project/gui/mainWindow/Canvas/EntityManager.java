package com.comp3222.project.gui.mainWindow.Canvas;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class EntityManager {
  private static EntityManager instance;
  private List<Entity> grids = new ArrayList<>();
  private int gridLength = 60;
  private GraphicsContext gc;
  private Boolean debug;
  private Canvas canvas;

  private EntityManager(int gridLength, GraphicsContext gc, Canvas canvas, Boolean debug) {
    this.gridLength = gridLength;
    this.gc = gc;
    this.debug = debug;
    this.canvas = canvas;
  }

  public static EntityManager getInstance(
      int gridLength, GraphicsContext gc, Canvas canvas, Boolean debug) {
    if (instance == null) instance = new EntityManager(gridLength, gc, canvas, debug);
    return instance;
  }

  public static EntityManager getInstance() {
    return instance;
  }

  public void resetAll() {
    grids.clear();
  }

  public double point(double x) {
    return x * gridLength;
  }

  public void add(Entity grid) {
    grids.add(grid);
    sort();
  }

  public void sort() {
    grids.sort((o1, o2) -> o1.getLayer() - o2.getLayer());
  }

  public Optional<Entity> getChessByXY(Double x, Double y) {
    return grids
        .stream()
        .filter(
            entity ->
                entity instanceof ChessEntity
                    && ((ChessEntity) entity).getChess().isCurrentPlayer()
                    && x >= entity.getX()
                    && x <= (entity.getX() + entity.getW())
                    && y >= entity.getY()
                    && y <= (entity.getY() + entity.getH()))
        .findFirst();
  }

  public Optional<Entity> getBoardGridByXY(Double x, Double y) {
    return grids
        .stream()
        .filter(
            entity ->
                entity instanceof BoardGrid
                    && x >= entity.getX()
                    && x <= (entity.getX() + entity.getW())
                    && y >= entity.getY()
                    && y <= (entity.getY() + entity.getH()))
        .findFirst();
  }

  public Stream<Entity> getAllHighlightedChessEntities() {
    return grids
        .stream()
        .filter(entity -> entity instanceof ChessEntity && ((ChessEntity) entity).getHighlight());
  }

  public Optional<Entity> getMovingChessEntity() {
    return grids
        .stream()
        .filter(
            entity ->
                entity instanceof ChessEntity
                    && ((ChessEntity) entity).getChessEntityStatus() == ChessEntityStatus.Moving)
        .findFirst();
  }

  public Optional<Entity> getGridEntity(String gridId) {
    return grids
        .stream()
        .filter(
            entity ->
                entity instanceof BoardGrid
                    && ((BoardGrid) entity).getGrid().getId().equals(gridId))
        .findFirst();
  }

  public void reDrawWholeScreen() {
    if (debug) {
      drawGrid();
    }

    grids.forEach(
        grid -> {
          try {
            grid.draw();
          } catch (Exception e) {
            e.printStackTrace();
          }
        });
  }

  public void drawGrid() {
    double bw = canvas.getWidth();
    double bh = canvas.getHeight();
    int p = 0;

    for (int x = 0; x <= bw; x += gridLength) {
      gc.moveTo(0.5 + x + p, p);
      gc.lineTo(0.5 + x + p, bh + p);
    }

    for (int x = 0; x <= bh; x += gridLength) {
      gc.moveTo(p, 0.5 + x + p);
      gc.lineTo(bw + p, 0.5 + x + p);
    }

    gc.setStroke(Color.BLACK);
    gc.stroke();
  }
}
