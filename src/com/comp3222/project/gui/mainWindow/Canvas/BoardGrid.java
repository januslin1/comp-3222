package com.comp3222.project.gui.mainWindow.Canvas;

import com.comp3222.project.core.board.Grid;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class BoardGrid extends Entity {
  private Grid grid;
  private Image image;
  private boolean highlight = false;

  public BoardGrid(
      double x,
      double y,
      double w,
      double h,
      int layer,
      GraphicsContext gc,
      Grid grid)
      throws Exception {
    super(x, y, w, h, layer, gc);
    this.grid = grid;

    // File file = new File(grid.imagePath());
    // FileInputStream input = new FileInputStream(file);
    image = new Image(grid.imagePath());
  }

  public Grid getGrid() {
    return grid;
  }

  @Override
  public void draw() {
    getGc().drawImage(image, getX(), getY(), getW(), getH());

    if (highlight) {
      highlight();
    }
  }

  public boolean isHighlight() {
    return highlight;
  }

  public void setHighlight(boolean highlight) {
    this.highlight = highlight;
  }

  private void highlight() {
    getGc().setGlobalAlpha(0.3);
    getGc().setFill(Color.YELLOW);
    getGc().fillRect(getX(), getY(), getW(), getH());
    getGc().setGlobalAlpha(1);
    getGc().setFill(Color.BLACK);
  }
}
