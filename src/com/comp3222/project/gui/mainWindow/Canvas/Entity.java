package com.comp3222.project.gui.mainWindow.Canvas;

import javafx.scene.canvas.GraphicsContext;

public abstract class Entity {
  private double x;
  private double y;
  private double w;
  private double h;
  private int layer;
  private GraphicsContext gc;

  Entity(double x, double y, double w, double h, int layer, GraphicsContext gc) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.layer = layer;
    this.gc = gc;
  }

  public double getX() {
    return x;
  }

  void setX(double x) {
    this.x = x;
  }

  public double getY() {
    return y;
  }

  void setY(double y) {
    this.y = y;
  }

  public double getW() {
    return w;
  }

  public double getH() {
    return h;
  }

  public int getLayer() {
    return layer;
  }

  void setLayer(int layer) {
    this.layer = layer;
  }

  GraphicsContext getGc() {
    return gc;
  }

  public abstract void draw() throws Exception;

  protected double point(double x) {
    return EntityManager.getInstance().point(x);
  }
}
