package com.comp3222.project;

import com.comp3222.project.core.Application;
import com.comp3222.project.core.Registry;
import com.comp3222.project.core.board.*;
import com.comp3222.project.core.chess.*;

public class Main extends Application {
  public static final String app_name = "Jungle Game";
  public static final String version = "0.1.0";
  static final String HELP_MESSAGE =
      app_name
          + " ("
          + version
          + ")\n"
          + "Example: `<program> -h` for help message\n"
          + "\n"
          + "Example: `<program> -g` for graphical mode";

  /**
   * Start the application
   *
   * @param args argument
   */
  public static void main(String[] args) {
    Main app = new Main();
    System.out.println(app_name + " (" + version + ")");

    // check start gui mode or cli mode
    if (args.length > 0
        && (args[0].toLowerCase().equals("-g") || args[0].toLowerCase().equals("--graphic"))) {
      app.startGUIMode();
    } else if (args.length > 0
        && (args[0].toLowerCase().equals("-h") || args[0].toLowerCase().equals("--help"))) {
      System.out.println(HELP_MESSAGE);
    } else {
      app.startCMLMode();
    }
  }

  @Override
  public void gridRegistry() {
    // Grid Registry
    Registry.gridRegister("normal", NormalGrid.class);
    Registry.gridRegister("trapA", TrapA.class);
    Registry.gridRegister("trapB", TrapB.class);
    Registry.gridRegister("river", River.class);
    Registry.gridRegister("denA", DenA.class);
    Registry.gridRegister("denB", DenB.class);
  }

  @Override
  public void boardRegistry() {
    // Board Map Registry
    Registry.boardRegister("1A", "normal", "2A", "1B", true);
    Registry.boardRegister("1B", "normal", "2B", "1C", false);
    Registry.boardRegister("1C", "trapA", "2C", "1D", false);
    Registry.boardRegister("1D", "denA", "2D", "1E", false);
    Registry.boardRegister("1E", "trapA", "2E", "1F", false);
    Registry.boardRegister("1F", "normal", "2F", "1G", false);
    Registry.boardRegister("1G", "normal", "2G", null, false);

    Registry.boardRegister("2A", "normal", "3A", "2B", false);
    Registry.boardRegister("2B", "normal", "3B", "2C", false);
    Registry.boardRegister("2C", "normal", "3C", "2D", false);
    Registry.boardRegister("2D", "trapA", "3D", "2E", false);
    Registry.boardRegister("2E", "normal", "3E", "2F", false);
    Registry.boardRegister("2F", "normal", "3F", "2G", false);
    Registry.boardRegister("2G", "normal", "3G", null, false);

    Registry.boardRegister("3A", "normal", "4A", "3B", false);
    Registry.boardRegister("3B", "normal", "4B", "3C", false);
    Registry.boardRegister("3C", "normal", "4C", "3D", false);
    Registry.boardRegister("3D", "normal", "4D", "3E", false);
    Registry.boardRegister("3E", "normal", "4E", "3F", false);
    Registry.boardRegister("3F", "normal", "4F", "3G", false);
    Registry.boardRegister("3G", "normal", "4G", null, false);

    Registry.boardRegister("4A", "normal", "5A", "4B", false);
    Registry.boardRegister("4B", "river", "5B", "4C", false);
    Registry.boardRegister("4C", "river", "5C", "4D", false);
    Registry.boardRegister("4D", "normal", "5D", "4E", false);
    Registry.boardRegister("4E", "river", "5E", "4F", false);
    Registry.boardRegister("4F", "river", "5F", "4G", false);
    Registry.boardRegister("4G", "normal", "5G", null, false);

    Registry.boardRegister("5A", "normal", "6A", "5B", false);
    Registry.boardRegister("5B", "river", "6B", "5C", false);
    Registry.boardRegister("5C", "river", "6C", "5D", false);
    Registry.boardRegister("5D", "normal", "6D", "5E", false);
    Registry.boardRegister("5E", "river", "6E", "5F", false);
    Registry.boardRegister("5F", "river", "6F", "5G", false);
    Registry.boardRegister("5G", "normal", "6G", null, false);

    Registry.boardRegister("6A", "normal", "7A", "6B", false);
    Registry.boardRegister("6B", "river", "7B", "6C", false);
    Registry.boardRegister("6C", "river", "7C", "6D", false);
    Registry.boardRegister("6D", "normal", "7D", "6E", false);
    Registry.boardRegister("6E", "river", "7E", "6F", false);
    Registry.boardRegister("6F", "river", "7F", "6G", false);
    Registry.boardRegister("6G", "normal", "7G", null, false);

    Registry.boardRegister("7A", "normal", "8A", "7B", false);
    Registry.boardRegister("7B", "normal", "8B", "7C", false);
    Registry.boardRegister("7C", "normal", "8C", "7D", false);
    Registry.boardRegister("7D", "normal", "8D", "7E", false);
    Registry.boardRegister("7E", "normal", "8E", "7F", false);
    Registry.boardRegister("7F", "normal", "8F", "7G", false);
    Registry.boardRegister("7G", "normal", "8G", null, false);

    Registry.boardRegister("8A", "normal", "9A", "8B", false);
    Registry.boardRegister("8B", "normal", "9B", "8C", false);
    Registry.boardRegister("8C", "normal", "9C", "8D", false);
    Registry.boardRegister("8D", "trapB", "9D", "8E", false);
    Registry.boardRegister("8E", "normal", "9E", "8F", false);
    Registry.boardRegister("8F", "normal", "9F", "8G", false);
    Registry.boardRegister("8G", "normal", "9G", null, false);

    Registry.boardRegister("9A", "normal", null, "9B", false);
    Registry.boardRegister("9B", "normal", null, "9C", false);
    Registry.boardRegister("9C", "trapB", null, "9D", false);
    Registry.boardRegister("9D", "denB", null, "9E", false);
    Registry.boardRegister("9E", "trapB", null, "9F", false);
    Registry.boardRegister("9F", "normal", null, "9G", false);
    Registry.boardRegister("9G", "normal", null, null, false);
  }

  @Override
  public void chessRegistry() {
    // Chess Registry
    Registry.chessRegister(Mouse.class, "3A", "7G", "mouseA.png", "mouseB.png");
    Registry.chessRegister(Cat.class, "2F", "8B", "catA.png", "catB.png");
    Registry.chessRegister(Wolf.class, "3E", "7C", "wolfA.png", "wolfB.png");
    Registry.chessRegister(Dog.class, "2B", "8F", "dogA.png", "dogB.png");
    Registry.chessRegister(Leopard.class, "3C", "7E", "leopardA.png", "leopardB.png");
    Registry.chessRegister(Tiger.class, "1A", "9G", "tigerA.png", "tigerB.png");
    Registry.chessRegister(Lion.class, "1G", "9A", "lionA.png", "lionB.png");
    Registry.chessRegister(Elephant.class, "3G", "7A", "elephantA.png", "elephantB.png");
  }
}
